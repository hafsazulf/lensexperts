<?php

/**
 * This is the model class for table "wishlist_item".
 *
 * The followings are the available columns in table 'wishlist_item':
 * @property integer $wishlist_item_id
 * @property integer $wishlist_id
 * @property integer $product_id
 * @property integer $store_id
 * @property string $added_at
 * @property string $description
 * @property integer $qty
 */
class Mage2WishlistItem extends Mage2ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wishlist_item}}';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('wishlist_id, product_id, store_id', 'numerical', 'integerOnly'=>true),
            array('qty', 'length', 'max' => 12)
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage2WishlistItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
