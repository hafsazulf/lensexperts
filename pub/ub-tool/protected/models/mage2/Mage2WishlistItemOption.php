<?php

/**
 * This is the model class for table "wishlist_item_option".
 *
 * The followings are the available columns in table 'wishlist_item_option':
 * @property integer $option_id
 * @property integer$wishlist_item_id
 * @property integer $product_id
 * @property string $code
 * @property string $value
 */
class Mage2WishlistItemOption extends Mage2ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wishlist_item_option}}';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('wishlist_item_id, product_id', 'numerical', 'integerOnly'=>true),
            array('code', 'length', 'max' => 255)
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage2WishlistItemOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
