<?php

/**
 * This is the model class for table "catalog_product_option_type_price".
 *
 * The followings are the available columns in table 'catalog_product_option_type_price':
 * @property string $option_type_price_id
 * @property string $option_type_id
 * @property integer $store_id
 * @property string $price
 * @property string $price_type
 */
class Mage1CatalogProductOptionTypePrice extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_option_type_price}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductOptionTypePrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
