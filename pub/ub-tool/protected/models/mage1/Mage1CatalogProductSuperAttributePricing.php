<?php

/**
 * This is the model class for table "catalog_product_super_attribute_pricing".
 *
 * The followings are the available columns in table 'catalog_product_super_attribute_pricing':
 * @property string $value_id
 * @property string $product_super_attribute_id
 * @property string $value_index
 * @property integer $is_percent
 * @property string $pricing_value
 * @property integer $website_id
 */
class Mage1CatalogProductSuperAttributePricing extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_super_attribute_pricing}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductSuperAttributePricing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
