<?php

/**
 * This is the model class for table "sales_flat_quote".
 *
 * The followings are the available columns in table 'sales_flat_quote':
 * @property string $entity_id
 * @property integer $store_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $converted_at
 * @property integer $is_active
 * @property integer $is_virtual
 * @property integer $is_multi_shipping
 * @property string $items_count
 * @property string $items_qty
 * @property string $orig_order_id
 * @property string $store_to_base_rate
 * @property string $store_to_quote_rate
 * @property string $base_currency_code
 * @property string $store_currency_code
 * @property string $quote_currency_code
 * @property string $grand_total
 * @property string $base_grand_total
 * @property string $checkout_method
 * @property string $customer_id
 * @property string $customer_tax_class_id
 * @property string $customer_group_id
 * @property string $customer_email
 * @property string $customer_prefix
 * @property string $customer_firstname
 * @property string $customer_middlename
 * @property string $customer_lastname
 * @property string $customer_suffix
 * @property string $customer_dob
 * @property string $customer_note
 * @property integer $customer_note_notify
 * @property integer $customer_is_guest
 * @property string $remote_ip
 * @property string $applied_rule_ids
 * @property string $reserved_order_id
 * @property string $password_hash
 * @property string $coupon_code
 * @property string $global_currency_code
 * @property string $base_to_global_rate
 * @property string $base_to_quote_rate
 * @property string $customer_taxvat
 * @property string $customer_gender
 * @property string $subtotal
 * @property string $base_subtotal
 * @property string $subtotal_with_discount
 * @property string $base_subtotal_with_discount
 * @property string $is_changed
 * @property integer $trigger_recollect
 * @property string $ext_shipping_info
 * @property integer $gift_message_id
 * @property integer $is_persistent
 * @property string $customer_balance_amount_used
 * @property string $base_customer_bal_amount_used
 * @property integer $use_customer_balance
 * @property string $gift_cards
 * @property string $gift_cards_amount
 * @property string $base_gift_cards_amount
 * @property string $gift_cards_amount_used
 * @property string $base_gift_cards_amount_used
 * @property integer $gw_id
 * @property integer $gw_allow_gift_receipt
 * @property integer $gw_add_card
 * @property string $gw_base_price
 * @property string $gw_price
 * @property string $gw_items_base_price
 * @property string $gw_items_price
 * @property string $gw_card_base_price
 * @property string $gw_card_price
 * @property string $gw_base_tax_amount
 * @property string $gw_tax_amount
 * @property string $gw_items_base_tax_amount
 * @property string $gw_items_tax_amount
 * @property string $gw_card_base_tax_amount
 * @property string $gw_card_tax_amount
 * @property integer $use_reward_points
 * @property integer $reward_points_balance
 * @property string $base_reward_currency_amount
 * @property string $reward_currency_amount
 */
class Mage1SalesQuote extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_quote}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesQuote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
