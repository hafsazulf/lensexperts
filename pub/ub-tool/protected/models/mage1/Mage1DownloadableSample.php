<?php

/**
 * This is the model class for table "downloadable_sample".
 *
 * The followings are the available columns in table 'downloadable_sample':
 * @property string $sample_id
 * @property string $product_id
 * @property string $sample_url
 * @property string $sample_file
 * @property string $sample_type
 * @property string $sort_order
 */
class Mage1DownloadableSample extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{downloadable_sample}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1DownloadableSample the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
