<?php

/**
 * This is the model class for table "catalog_category_entity".
 *
 * The followings are the available columns in table 'catalog_category_entity':
 * @property string $entity_id
 * @property integer $entity_type_id
 * @property integer $attribute_set_id
 * @property string $parent_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $path
 * @property integer $position
 * @property integer $level
 * @property integer $children_count
 */
class Mage1CatalogCategoryEntity extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_category_entity}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogCategoryEntity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
