<?php

/**
 * This is the model class for table "newsletter_queue_link".
 *
 * The followings are the available columns in table 'newsletter_queue_link':
 * @property string $queue_link_id
 * @property string $queue_id
 * @property string $subscriber_id
 * @property string $letter_sent_at
 */
class Mage1NewsletterQueueLink extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{newsletter_queue_link}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1NewsletterQueueLink the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
