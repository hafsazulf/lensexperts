<?php

/**
 * This is the model class for table "core_url_rewrite".
 *
 * The followings are the available columns in table 'core_url_rewrite':
 * @property string $url_rewrite_id
 * @property integer $store_id
 * @property string $id_path
 * @property string $request_path
 * @property string $target_path
 * @property integer $is_system
 * @property string $options
 * @property string $description
 * @property string $category_id
 * @property string $product_id
 */
class Mage1UrlRewrite extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{core_url_rewrite}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1UrlRewrite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
