<?php

/**
 * This is the model class for table "downloadable_link".
 *
 * The followings are the available columns in table 'downloadable_link':
 * @property string $link_id
 * @property string $product_id
 * @property string $sort_order
 * @property integer $number_of_downloads
 * @property integer $is_shareable
 * @property string $link_url
 * @property string $link_file
 * @property string $link_type
 * @property string $sample_url
 * @property string $sample_file
 * @property string $sample_type
 */
class Mage1DownloadableLink extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{downloadable_link}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1DownloadableLink the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
