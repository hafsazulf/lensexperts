<?php

/**
 * This is the model class for table "catalogrule_group_website".
 *
 * The followings are the available columns in table 'catalogrule_group_website':
 * @property string $rule_id
 * @property integer $customer_group_id
 * @property integer $website_id
 */
class Mage1CatalogruleGroupWebsite extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalogrule_group_website}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogruleGroupWebsite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
