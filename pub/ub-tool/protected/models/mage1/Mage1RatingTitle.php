<?php

/**
 * This is the model class for table "rating_title".
 *
 * The followings are the available columns in table 'rating_title':
 * @property integer $rating_id
 * @property integer $store_id
 * @property string $value
 */
class Mage1RatingTitle extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rating_title}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1RatingTitle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
