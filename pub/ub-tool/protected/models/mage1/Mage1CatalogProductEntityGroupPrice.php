<?php

/**
 * This is the model class for table "catalog_product_entity_group_price".
 *
 * The followings are the available columns in table 'catalog_product_entity_group_price':
 * @property integer $value_id
 * @property string $entity_id
 * @property integer $all_groups
 * @property integer $customer_group_id
 * @property string $value
 * @property integer $website_id
 */
class Mage1CatalogProductEntityGroupPrice extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_entity_group_price}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductEntityGroupPrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
