<?php

/**
 * This is the model class for table "catalog_product_relation".
 *
 * The followings are the available columns in table 'catalog_product_relation':
 * @property string $parent_id
 * @property string $child_id
 */
class Mage1CatalogProductRelation extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_relation}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductRelation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
