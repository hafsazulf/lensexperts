<?php

/**
 * This is the model class for table "eav_attribute_label".
 *
 * The followings are the available columns in table 'eav_attribute_label':
 * @property string $attribute_label_id
 * @property integer $attribute_id
 * @property integer $store_id
 * @property string $value
 */
class Mage1AttributeLabel extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{eav_attribute_label}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1AttributeLabel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
