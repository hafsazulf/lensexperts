<?php

/**
 * This is the model class for table "tax_class".
 *
 * The followings are the available columns in table 'tax_class':
 * @property integer $class_id
 * @property string $class_name
 * @property string $class_type
 */
class Mage1TaxClass extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_class}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1TaxClass the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
