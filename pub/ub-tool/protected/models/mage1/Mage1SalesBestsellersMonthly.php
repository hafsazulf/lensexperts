<?php

/**
 * This is the model class for table "sales_bestsellers_aggregated_monthly".
 *
 * The followings are the available columns in table 'sales_bestsellers_aggregated_monthly':
 * @property string $id
 * @property string $period
 * @property integer $store_id
 * @property string $product_id
 * @property string $product_name
 * @property string $product_price
 * @property string $qty_ordered
 * @property integer $rating_pos
 */
class Mage1SalesBestsellersMonthly extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_bestsellers_aggregated_monthly}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesBestsellersMonthly the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
