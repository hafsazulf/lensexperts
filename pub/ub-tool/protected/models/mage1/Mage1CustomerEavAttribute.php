<?php

/**
 * This is the model class for table "customer_eav_attribute".
 *
 * The followings are the available columns in table 'customer_eav_attribute':
 * @property integer $attribute_id
 * @property integer $is_visible
 * @property string $input_filter
 * @property integer $multiline_count
 * @property string $validate_rules
 * @property integer $is_system
 * @property string $sort_order
 * @property string $data_model
 * @property string $is_used_for_customer_segment
 */
class Mage1CustomerEavAttribute extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customer_eav_attribute}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CustomerEavAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
