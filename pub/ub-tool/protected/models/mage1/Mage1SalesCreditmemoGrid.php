<?php

/**
 * This is the model class for table "sales_flat_creditmemo_grid".
 *
 * The followings are the available columns in table 'sales_flat_creditmemo_grid':
 * @property string $entity_id
 * @property integer $store_id
 * @property string $store_to_order_rate
 * @property string $base_to_order_rate
 * @property string $grand_total
 * @property string $store_to_base_rate
 * @property string $base_to_global_rate
 * @property string $base_grand_total
 * @property string $order_id
 * @property integer $creditmemo_status
 * @property integer $state
 * @property integer $invoice_id
 * @property string $store_currency_code
 * @property string $order_currency_code
 * @property string $base_currency_code
 * @property string $global_currency_code
 * @property string $increment_id
 * @property string $order_increment_id
 * @property string $created_at
 * @property string $order_created_at
 * @property string $billing_name
 */
class Mage1SalesCreditmemoGrid extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_creditmemo_grid}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesCreditmemoGrid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
