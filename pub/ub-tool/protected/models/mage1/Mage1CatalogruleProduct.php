<?php

/**
 * This is the model class for table "catalogrule_product".
 *
 * The followings are the available columns in table 'catalogrule_product':
 * @property string $rule_product_id
 * @property string $rule_id
 * @property string $from_time
 * @property string $to_time
 * @property integer $customer_group_id
 * @property string $product_id
 * @property string $action_operator
 * @property string $action_amount
 * @property integer $action_stop
 * @property string $sort_order
 * @property integer $website_id
 * @property string $sub_simple_action
 * @property string $sub_discount_amount
 */
class Mage1CatalogruleProduct extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalogrule_product}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogruleProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
