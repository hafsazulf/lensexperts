<?php

/**
 * This is the model class for table "sales_flat_order_item".
 *
 * The followings are the available columns in table 'sales_flat_order_item':
 * @property string $item_id
 * @property string $order_id
 * @property string $parent_item_id
 * @property string $quote_item_id
 * @property integer $store_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $product_id
 * @property string $product_type
 * @property string $product_options
 * @property string $weight
 * @property integer $is_virtual
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property string $applied_rule_ids
 * @property string $additional_data
 * @property integer $free_shipping
 * @property integer $is_qty_decimal
 * @property integer $no_discount
 * @property string $qty_backordered
 * @property string $qty_canceled
 * @property string $qty_invoiced
 * @property string $qty_ordered
 * @property string $qty_refunded
 * @property string $qty_shipped
 * @property string $base_cost
 * @property string $price
 * @property string $base_price
 * @property string $original_price
 * @property string $base_original_price
 * @property string $tax_percent
 * @property string $tax_amount
 * @property string $base_tax_amount
 * @property string $tax_invoiced
 * @property string $base_tax_invoiced
 * @property string $discount_percent
 * @property string $discount_amount
 * @property string $base_discount_amount
 * @property string $discount_invoiced
 * @property string $base_discount_invoiced
 * @property string $amount_refunded
 * @property string $base_amount_refunded
 * @property string $row_total
 * @property string $base_row_total
 * @property string $row_invoiced
 * @property string $base_row_invoiced
 * @property string $row_weight
 * @property string $base_tax_before_discount
 * @property string $tax_before_discount
 * @property string $ext_order_item_id
 * @property integer $locked_do_invoice
 * @property integer $locked_do_ship
 * @property string $price_incl_tax
 * @property string $base_price_incl_tax
 * @property string $row_total_incl_tax
 * @property string $base_row_total_incl_tax
 * @property string $hidden_tax_amount
 * @property string $base_hidden_tax_amount
 * @property string $hidden_tax_invoiced
 * @property string $base_hidden_tax_invoiced
 * @property string $hidden_tax_refunded
 * @property string $base_hidden_tax_refunded
 * @property integer $is_nominal
 * @property string $tax_canceled
 * @property string $hidden_tax_canceled
 * @property string $tax_refunded
 * @property string $base_tax_refunded
 * @property string $discount_refunded
 * @property string $base_discount_refunded
 * @property integer $gift_message_id
 * @property integer $gift_message_available
 * @property string $base_weee_tax_applied_amount
 * @property string $base_weee_tax_applied_row_amnt
 * @property string $weee_tax_applied_amount
 * @property string $weee_tax_applied_row_amount
 * @property string $weee_tax_applied
 * @property string $weee_tax_disposition
 * @property string $weee_tax_row_disposition
 * @property string $base_weee_tax_disposition
 * @property string $base_weee_tax_row_disposition
 * @property integer $event_id
 * @property integer $giftregistry_item_id
 * @property integer $gw_id
 * @property string $gw_base_price
 * @property string $gw_price
 * @property string $gw_base_tax_amount
 * @property string $gw_tax_amount
 * @property string $gw_base_price_invoiced
 * @property string $gw_price_invoiced
 * @property string $gw_base_tax_amount_invoiced
 * @property string $gw_tax_amount_invoiced
 * @property string $gw_base_price_refunded
 * @property string $gw_price_refunded
 * @property string $gw_base_tax_amount_refunded
 * @property string $gw_tax_amount_refunded
 * @property string $qty_returned
 */
class Mage1SalesOrderItem extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_order_item}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
