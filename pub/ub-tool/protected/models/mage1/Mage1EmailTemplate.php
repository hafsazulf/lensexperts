<?php

/**
 * This is the model class for table "core_email_template".
 *
 * The followings are the available columns in table 'core_email_template':
 * @property string $template_id
 * @property string $template_code
 * @property string $template_text
 * @property string $template_styles
 * @property string $template_type
 * @property string $template_subject
 * @property string $template_sender_name
 * @property string $template_sender_email
 * @property string $added_at
 * @property string $modified_at
 * @property string $orig_template_code
 * @property string $orig_template_variables
 */
class Mage1EmailTemplate extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{core_email_template}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1EmailTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
