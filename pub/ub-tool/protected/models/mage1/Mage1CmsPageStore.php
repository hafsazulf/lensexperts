<?php

/**
 * This is the model class for table "cms_page_store".
 *
 * The followings are the available columns
 * @property string $page_id
 * @property string $store_id
 */
class Mage1CmsPageStore extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cms_page_store}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CmsPageStore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
