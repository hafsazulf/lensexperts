<?php

/**
 * This is the model class for table "tax_calculation".
 *
 * The followings are the available columns in table 'tax_calculation':
 * @property integer $tax_calculation_id
 * @property integer $tax_calculation_rate_id
 * @property integer $tax_calculation_rule_id
 * @property integer $customer_tax_class_id
 * @property integer $product_tax_class_id
 */
class Mage1TaxCalculation extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_calculation}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1TaxCalculation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
