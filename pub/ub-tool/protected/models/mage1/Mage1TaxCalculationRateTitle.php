<?php

/**
 * This is the model class for table "tax_calculation_rate_title".
 *
 * The followings are the available columns in table 'tax_calculation_rate_title':
 * @property integer $tax_calculation_rate_title_id
 * @property integer $tax_calculation_rate_id
 * @property integer $store_id
 * @property string $value
 */
class Mage1TaxCalculationRateTitle extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_calculation_rate_title}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1TaxCalculationRateTitle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
