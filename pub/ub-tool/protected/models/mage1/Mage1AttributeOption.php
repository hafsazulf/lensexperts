<?php

/**
 * This is the model class for table "eav_attribute_option".
 *
 * The followings are the available columns in table 'eav_attribute_option':
 * @property string $option_id
 * @property integer $attribute_id
 * @property integer $sort_order
 */
class Mage1AttributeOption extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{eav_attribute_option}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1AttributeOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
