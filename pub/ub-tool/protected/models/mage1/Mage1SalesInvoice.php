<?php

/**
 * This is the model class for table "sales_flat_invoice".
 *
 * The followings are the available columns in table 'sales_flat_invoice':
 * @property string $entity_id
 * @property integer $store_id
 * @property string $base_grand_total
 * @property string $shipping_tax_amount
 * @property string $tax_amount
 * @property string $base_tax_amount
 * @property string $store_to_order_rate
 * @property string $base_shipping_tax_amount
 * @property string $base_discount_amount
 * @property string $base_to_order_rate
 * @property string $grand_total
 * @property string $shipping_amount
 * @property string $subtotal_incl_tax
 * @property string $base_subtotal_incl_tax
 * @property string $store_to_base_rate
 * @property string $base_shipping_amount
 * @property string $total_qty
 * @property string $base_to_global_rate
 * @property string $subtotal
 * @property string $base_subtotal
 * @property string $discount_amount
 * @property integer $billing_address_id
 * @property integer $is_used_for_refund
 * @property string $order_id
 * @property integer $email_sent
 * @property integer $can_void_flag
 * @property integer $state
 * @property integer $shipping_address_id
 * @property string $store_currency_code
 * @property string $transaction_id
 * @property string $order_currency_code
 * @property string $base_currency_code
 * @property string $global_currency_code
 * @property string $increment_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $hidden_tax_amount
 * @property string $base_hidden_tax_amount
 * @property string $shipping_hidden_tax_amount
 * @property string $base_shipping_hidden_tax_amnt
 * @property string $shipping_incl_tax
 * @property string $base_shipping_incl_tax
 * @property string $base_total_refunded
 * @property string $base_customer_balance_amount
 * @property string $customer_balance_amount
 * @property string $base_gift_cards_amount
 * @property string $gift_cards_amount
 * @property string $gw_base_price
 * @property string $gw_price
 * @property string $gw_items_base_price
 * @property string $gw_items_price
 * @property string $gw_card_base_price
 * @property string $gw_card_price
 * @property string $gw_base_tax_amount
 * @property string $gw_tax_amount
 * @property string $gw_items_base_tax_amount
 * @property string $gw_items_tax_amount
 * @property string $gw_card_base_tax_amount
 * @property string $gw_card_tax_amount
 * @property string $base_reward_currency_amount
 * @property string $reward_currency_amount
 * @property integer $reward_points_balance
 * @property string $discount_description
 * @property string $cybersource_token
 */
class Mage1SalesInvoice extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_invoice}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesInvoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
