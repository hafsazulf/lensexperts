<?php

/**
 * This is the model class for table "catalogrule_product_price".
 *
 * The followings are the available columns in table 'catalogrule_product_price':
 * @property string $rule_product_price_id
 * @property string $rule_date
 * @property integer $customer_group_id
 * @property string $product_id
 * @property string $rule_price
 * @property integer $website_id
 * @property string $latest_start_date
 * @property string $earliest_end_date
 */
class Mage1CatalogruleProductPrice extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalogrule_product_price}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogruleProductPrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
