<?php

/**
 * This is the model class for table "review_detail".
 *
 * The followings are the available columns in table 'review_detail':
 * @property string $detail_id
 * @property string $review_id
 * @property integer $store_id
 * @property string $title
 * @property string $detail
 * @property string $nickname
 * @property string $customer_id
 */
class Mage1ReviewDetail extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{review_detail}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1ReviewDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
