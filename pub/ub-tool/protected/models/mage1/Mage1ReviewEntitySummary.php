<?php

/**
 * This is the model class for table "review_entity_summary".
 *
 * The followings are the available columns in table 'review_entity_summary':
 * @property string $primary_id
 * @property string $entity_pk_value
 * @property integer $entity_type
 * @property integer $reviews_count
 * @property integer $rating_summary
 * @property integer $store_id
 */
class Mage1ReviewEntitySummary extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{review_entity_summary}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1ReviewEntitySummary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
