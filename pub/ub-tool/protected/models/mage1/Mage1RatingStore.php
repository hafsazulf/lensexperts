<?php

/**
 * This is the model class for table "rating_store".
 *
 * The followings are the available columns in table 'rating_store':
 * @property integer $rating_id
 * @property integer $store_id
 */
class Mage1RatingStore extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rating_store}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1RatingStore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
