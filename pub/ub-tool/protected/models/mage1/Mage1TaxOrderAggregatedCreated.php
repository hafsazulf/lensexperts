<?php

/**
 * This is the model class for table "tax_order_aggregated_created".
 *
 * The followings are the available columns in table 'tax_order_aggregated_created':
 * @property string $id
 * @property string $period
 * @property integer $store_id
 * @property string $code
 * @property string $order_status
 * @property double $percent
 * @property string $orders_count
 * @property double $tax_base_amount_sum
 */
class Mage1TaxOrderAggregatedCreated extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_order_aggregated_created}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1TaxOrderAggregatedCreated the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
