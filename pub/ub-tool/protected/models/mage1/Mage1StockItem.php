<?php

/**
 * This is the model class for table "cataloginventory_stock_item".
 *
 * The followings are the available columns in table 'cataloginventory_stock_item':
 * @property string $item_id
 * @property string $product_id
 * @property integer $stock_id
 * @property string $qty
 * @property string $min_qty
 * @property integer $use_config_min_qty
 * @property integer $is_qty_decimal
 * @property integer $backorders
 * @property integer $use_config_backorders
 * @property string $min_sale_qty
 * @property integer $use_config_min_sale_qty
 * @property string $max_sale_qty
 * @property integer $use_config_max_sale_qty
 * @property integer $is_in_stock
 * @property string $low_stock_date
 * @property string $notify_stock_qty
 * @property integer $use_config_notify_stock_qty
 * @property integer $manage_stock
 * @property integer $use_config_manage_stock
 * @property integer $stock_status_changed_auto
 * @property integer $use_config_qty_increments
 * @property string $qty_increments
 * @property integer $use_config_enable_qty_inc
 * @property integer $enable_qty_increments
 * @property integer $is_decimal_divided
 */
class Mage1StockItem extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cataloginventory_stock_item}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1StockItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
