<?php

/**
 * This is the model class for table "tax_calculation_rate".
 *
 * The followings are the available columns in table 'tax_calculation_rate':
 * @property integer $tax_calculation_rate_id
 * @property string $tax_country_id
 * @property integer $tax_region_id
 * @property string $tax_postcode
 * @property string $code
 * @property string $rate
 * @property integer $zip_is_range
 * @property string $zip_from
 * @property string $zip_to
 */
class Mage1TaxCalculationRate extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_calculation_rate}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1TaxCalculationRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
