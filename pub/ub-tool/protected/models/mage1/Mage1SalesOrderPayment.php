<?php

/**
 * This is the model class for table "sales_flat_order_payment".
 *
 * The followings are the available columns in table 'sales_flat_order_payment':
 * @property string $entity_id
 * @property string $parent_id
 * @property string $base_shipping_captured
 * @property string $shipping_captured
 * @property string $amount_refunded
 * @property string $base_amount_paid
 * @property string $amount_canceled
 * @property string $base_amount_authorized
 * @property string $base_amount_paid_online
 * @property string $base_amount_refunded_online
 * @property string $base_shipping_amount
 * @property string $shipping_amount
 * @property string $amount_paid
 * @property string $amount_authorized
 * @property string $base_amount_ordered
 * @property string $base_shipping_refunded
 * @property string $shipping_refunded
 * @property string $base_amount_refunded
 * @property string $amount_ordered
 * @property string $base_amount_canceled
 * @property integer $quote_payment_id
 * @property string $additional_data
 * @property string $cc_exp_month
 * @property string $cc_ss_start_year
 * @property string $echeck_bank_name
 * @property string $method
 * @property string $cc_debug_request_body
 * @property string $cc_secure_verify
 * @property string $protection_eligibility
 * @property string $cc_approval
 * @property string $cc_last4
 * @property string $cc_status_description
 * @property string $echeck_type
 * @property string $cc_debug_response_serialized
 * @property string $cc_ss_start_month
 * @property string $echeck_account_type
 * @property string $last_trans_id
 * @property string $cc_cid_status
 * @property string $cc_owner
 * @property string $cc_type
 * @property string $po_number
 * @property string $cc_exp_year
 * @property string $cc_status
 * @property string $echeck_routing_number
 * @property string $account_status
 * @property string $anet_trans_method
 * @property string $cc_debug_response_body
 * @property string $cc_ss_issue
 * @property string $echeck_account_name
 * @property string $cc_avs_status
 * @property string $cc_number_enc
 * @property string $cc_trans_id
 * @property string $paybox_request_number
 * @property string $address_status
 * @property string $additional_information
 * @property string $cybersource_token
 * @property string $flo2cash_account_id
 * @property string $ideal_issuer_id
 * @property string $ideal_issuer_title
 * @property integer $ideal_transaction_checked
 * @property string $paybox_question_number
 */
class Mage1SalesOrderPayment extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_order_payment}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
