<?php

/**
 * This is the model class for table "sales_flat_shipment".
 *
 * The followings are the available columns in table 'sales_flat_shipment':
 * @property string $entity_id
 * @property integer $store_id
 * @property string $total_weight
 * @property string $total_qty
 * @property integer $email_sent
 * @property string $order_id
 * @property integer $customer_id
 * @property integer $shipping_address_id
 * @property integer $billing_address_id
 * @property integer $shipment_status
 * @property string $increment_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $packages
 * @property string $shipping_label
 */
class Mage1SalesShipment extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_shipment}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesFlatShipment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
