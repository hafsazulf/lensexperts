<?php

/**
 * This is the model class for table "sales_flat_quote_item_option".
 *
 * The followings are the available columns in table 'sales_flat_quote_item_option':
 * @property string $option_id
 * @property string $item_id
 * @property string $product_id
 * @property string $code
 * @property string $value
 */
class Mage1SalesQuoteItemOption extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_quote_item_option}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesQuoteItemOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
