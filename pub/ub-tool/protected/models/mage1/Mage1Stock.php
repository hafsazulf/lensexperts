<?php

/**
 * This is the model class for table "cataloginventory_stock".
 *
 * The followings are the available columns in table 'cataloginventory_stock':
 * @property integer $stock_id
 * @property string $stock_name
 */
class Mage1Stock extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cataloginventory_stock}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1Stock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
