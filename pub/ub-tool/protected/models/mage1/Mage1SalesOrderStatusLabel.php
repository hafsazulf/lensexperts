<?php

/**
 * This is the model class for table "sales_order_status_label".
 *
 * The followings are the available columns in table 'sales_order_status_label':
 * @property string $status
 * @property integer $store_id
 * @property string $label
 */
class Mage1SalesOrderStatusLabel extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_order_status_label}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderStatusLabel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
