<?php

/**
 * This is the model class for table "sales_flat_order_grid".
 *
 * The followings are the available columns in table 'sales_flat_order_grid':
 * @property string $entity_id
 * @property string $status
 * @property integer $store_id
 * @property string $store_name
 * @property string $customer_id
 * @property string $base_grand_total
 * @property string $base_total_paid
 * @property string $grand_total
 * @property string $total_paid
 * @property string $increment_id
 * @property string $base_currency_code
 * @property string $order_currency_code
 * @property string $shipping_name
 * @property string $billing_name
 * @property string $created_at
 * @property string $updated_at
 */
class Mage1SalesOrderGrid extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_order_grid}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderGrid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
