<?php

/**
 * This is the model class for table "sales_refunded_aggregated_order".
 *
 * The followings are the available columns in table 'sales_refunded_aggregated_order':
 * @property string $id
 * @property string $period
 * @property integer $store_id
 * @property string $order_status
 * @property integer $orders_count
 * @property string $refunded
 * @property string $online_refunded
 * @property string $offline_refunded
 */
class Mage1SalesRefundedAggregatedOrder extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_refunded_aggregated_order}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesRefundedAggregatedOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
