<?php

/**
 * This is the model class for table "catalog_product_bundle_selection_price".
 *
 * The followings are the available columns in table 'catalog_product_bundle_selection_price':
 * @property string $selection_id
 * @property integer $website_id
 * @property integer $selection_price_type
 * @property string $selection_price_value
 */
class Mage1CatalogProductBundleSelectionPrice extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_bundle_selection_price}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductBundleSelectionPrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
