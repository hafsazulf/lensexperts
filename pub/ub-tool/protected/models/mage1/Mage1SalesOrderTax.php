<?php

/**
 * This is the model class for table "sales_order_tax".
 *
 * The followings are the available columns in table 'sales_order_tax':
 * @property string $tax_id
 * @property string $order_id
 * @property string $code
 * @property string $title
 * @property string $percent
 * @property string $amount
 * @property integer $priority
 * @property integer $position
 * @property string $base_amount
 * @property integer $process
 * @property string $base_real_amount
 * @property integer $hidden
 */
class Mage1SalesOrderTax extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_order_tax}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderTax the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
