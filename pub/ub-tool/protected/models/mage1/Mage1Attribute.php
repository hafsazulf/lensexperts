<?php

/**
 * This is the model class for table "eav_attribute".
 *
 * The followings are the available columns in table 'eav_attribute':
 * @property integer $attribute_id
 * @property integer $entity_type_id
 * @property string $attribute_code
 * @property string $attribute_model
 * @property string $backend_model
 * @property string $backend_type
 * @property string $backend_table
 * @property string $frontend_model
 * @property string $frontend_input
 * @property string $frontend_label
 * @property string $frontend_class
 * @property string $source_model
 * @property integer $is_required
 * @property integer $is_user_defined
 * @property string $default_value
 * @property integer $is_unique
 * @property string $note
 */
class Mage1Attribute extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{eav_attribute}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1Attribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
