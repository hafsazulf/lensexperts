<?php

/**
 * This is the model class for table "catalogrule_customer_group".
 *
 * The followings are the available columns in table 'catalogrule_customer_group':
 * @property string $rule_id
 * @property integer $customer_group_id
 */
class Mage1CatalogruleCustomerGroup extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalogrule_customer_group}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogruleCustomerGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
