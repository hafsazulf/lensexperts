<?php

/**
 * This is the model class for table "sales_flat_shipment_item".
 *
 * The followings are the available columns in table 'sales_flat_shipment_item':
 * @property string $entity_id
 * @property string $parent_id
 * @property string $row_total
 * @property string $price
 * @property string $weight
 * @property string $qty
 * @property integer $product_id
 * @property integer $order_item_id
 * @property string $additional_data
 * @property string $description
 * @property string $name
 * @property string $sku
 */
class Mage1SalesShipmentItem extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_shipment_item}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesFlatShipmentItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
