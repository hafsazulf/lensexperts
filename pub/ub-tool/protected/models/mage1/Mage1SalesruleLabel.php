<?php

/**
 * This is the model class for table "salesrule_label".
 *
 * The followings are the available columns in table 'salesrule_label':
 * @property string $label_id
 * @property string $rule_id
 * @property integer $store_id
 * @property string $label
 */
class Mage1SalesruleLabel extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{salesrule_label}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesruleLabel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
