<?php

/**
 * This is the model class for table "catalog_product_website".
 *
 * The followings are the available columns in table 'catalog_product_website':
 * @property string $product_id
 * @property integer $website_id
 */
class Mage1CatalogProductWebsite extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_website}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductWebsite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
