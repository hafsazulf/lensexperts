<?php

/**
 * This is the model class for table "catalog_category_entity_datetime".
 *
 * The followings are the available columns in table 'catalog_category_entity_datetime':
 * @property integer $value_id
 * @property integer $entity_type_id
 * @property integer $attribute_id
 * @property integer $store_id
 * @property string $entity_id
 * @property string $value
 */
class Mage1CatalogCategoryEntityDatetime extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_category_entity_datetime}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogCategoryEntityDatetime the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
