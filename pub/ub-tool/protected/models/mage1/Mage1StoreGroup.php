<?php

/**
 * This is the model class for table "core_store_group".
 *
 * The followings are the available columns in table 'core_store_group':
 * @property integer $group_id
 * @property integer $website_id
 * @property string $name
 * @property string $root_category_id
 * @property integer $default_store_id
 */
class Mage1StoreGroup extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{core_store_group}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1StoreGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
