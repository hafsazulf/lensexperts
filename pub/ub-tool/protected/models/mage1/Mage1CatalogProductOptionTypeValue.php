<?php

/**
 * This is the model class for table "catalog_product_option_type_value".
 *
 * The followings are the available columns in table 'catalog_product_option_type_value':
 * @property string $option_type_id
 * @property string $option_id
 * @property string $sku
 * @property string $sort_order
 */
class Mage1CatalogProductOptionTypeValue extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_option_type_value}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductOptionTypeValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
