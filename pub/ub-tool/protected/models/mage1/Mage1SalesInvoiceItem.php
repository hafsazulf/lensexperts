<?php

/**
 * This is the model class for table "sales_flat_invoice_item".
 *
 * The followings are the available columns in table 'sales_flat_invoice_item':
 * @property string $entity_id
 * @property string $parent_id
 * @property string $base_price
 * @property string $tax_amount
 * @property string $base_row_total
 * @property string $discount_amount
 * @property string $row_total
 * @property string $base_discount_amount
 * @property string $price_incl_tax
 * @property string $base_tax_amount
 * @property string $base_price_incl_tax
 * @property string $qty
 * @property string $base_cost
 * @property string $price
 * @property string $base_row_total_incl_tax
 * @property string $row_total_incl_tax
 * @property integer $product_id
 * @property integer $order_item_id
 * @property string $additional_data
 * @property string $description
 * @property string $sku
 * @property string $name
 * @property string $hidden_tax_amount
 * @property string $base_hidden_tax_amount
 * @property string $base_weee_tax_applied_amount
 * @property string $base_weee_tax_applied_row_amnt
 * @property string $weee_tax_applied_amount
 * @property string $weee_tax_applied_row_amount
 * @property string $weee_tax_applied
 * @property string $weee_tax_disposition
 * @property string $weee_tax_row_disposition
 * @property string $base_weee_tax_disposition
 * @property string $base_weee_tax_row_disposition
 */
class Mage1SalesInvoiceItem extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_invoice_item}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesInvoiceItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
