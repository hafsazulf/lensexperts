<?php

/**
 * This is the model class for table "catalog_product_entity_decimal".
 *
 * The followings are the available columns in table 'catalog_product_entity_decimal':
 * @property integer $value_id
 * @property integer $entity_type_id
 * @property integer $attribute_id
 * @property integer $store_id
 * @property string $entity_id
 * @property string $value
 */
class Mage1CatalogProductEntityDecimal extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_entity_decimal}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductEntityDecimal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
