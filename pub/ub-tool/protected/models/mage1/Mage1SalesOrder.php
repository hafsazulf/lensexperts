<?php

/**
 * This is the model class for table "sales_flat_order".
 *
 * The followings are the available columns in table 'sales_flat_order':
 * @property string $entity_id
 * @property string $state
 * @property string $status
 * @property string $coupon_code
 * @property string $protect_code
 * @property string $shipping_description
 * @property integer $is_virtual
 * @property integer $store_id
 * @property string $customer_id
 * @property string $base_discount_amount
 * @property string $base_discount_canceled
 * @property string $base_discount_invoiced
 * @property string $base_discount_refunded
 * @property string $base_grand_total
 * @property string $base_shipping_amount
 * @property string $base_shipping_canceled
 * @property string $base_shipping_invoiced
 * @property string $base_shipping_refunded
 * @property string $base_shipping_tax_amount
 * @property string $base_shipping_tax_refunded
 * @property string $base_subtotal
 * @property string $base_subtotal_canceled
 * @property string $base_subtotal_invoiced
 * @property string $base_subtotal_refunded
 * @property string $base_tax_amount
 * @property string $base_tax_canceled
 * @property string $base_tax_invoiced
 * @property string $base_tax_refunded
 * @property string $base_to_global_rate
 * @property string $base_to_order_rate
 * @property string $base_total_canceled
 * @property string $base_total_invoiced
 * @property string $base_total_invoiced_cost
 * @property string $base_total_offline_refunded
 * @property string $base_total_online_refunded
 * @property string $base_total_paid
 * @property string $base_total_qty_ordered
 * @property string $base_total_refunded
 * @property string $discount_amount
 * @property string $discount_canceled
 * @property string $discount_invoiced
 * @property string $discount_refunded
 * @property string $grand_total
 * @property string $shipping_amount
 * @property string $shipping_canceled
 * @property string $shipping_invoiced
 * @property string $shipping_refunded
 * @property string $shipping_tax_amount
 * @property string $shipping_tax_refunded
 * @property string $store_to_base_rate
 * @property string $store_to_order_rate
 * @property string $subtotal
 * @property string $subtotal_canceled
 * @property string $subtotal_invoiced
 * @property string $subtotal_refunded
 * @property string $tax_amount
 * @property string $tax_canceled
 * @property string $tax_invoiced
 * @property string $tax_refunded
 * @property string $total_canceled
 * @property string $total_invoiced
 * @property string $total_offline_refunded
 * @property string $total_online_refunded
 * @property string $total_paid
 * @property string $total_qty_ordered
 * @property string $total_refunded
 * @property integer $can_ship_partially
 * @property integer $can_ship_partially_item
 * @property integer $customer_is_guest
 * @property integer $customer_note_notify
 * @property integer $billing_address_id
 * @property integer $customer_group_id
 * @property integer $edit_increment
 * @property integer $email_sent
 * @property integer $forced_shipment_with_invoice
 * @property integer $payment_auth_expiration
 * @property integer $quote_address_id
 * @property integer $quote_id
 * @property integer $shipping_address_id
 * @property string $adjustment_negative
 * @property string $adjustment_positive
 * @property string $base_adjustment_negative
 * @property string $base_adjustment_positive
 * @property string $base_shipping_discount_amount
 * @property string $base_subtotal_incl_tax
 * @property string $base_total_due
 * @property string $payment_authorization_amount
 * @property string $shipping_discount_amount
 * @property string $subtotal_incl_tax
 * @property string $total_due
 * @property string $weight
 * @property string $customer_dob
 * @property string $increment_id
 * @property string $applied_rule_ids
 * @property string $base_currency_code
 * @property string $customer_email
 * @property string $customer_firstname
 * @property string $customer_lastname
 * @property string $customer_middlename
 * @property string $customer_prefix
 * @property string $customer_suffix
 * @property string $customer_taxvat
 * @property string $discount_description
 * @property string $ext_customer_id
 * @property string $ext_order_id
 * @property string $global_currency_code
 * @property string $hold_before_state
 * @property string $hold_before_status
 * @property string $order_currency_code
 * @property string $original_increment_id
 * @property string $relation_child_id
 * @property string $relation_child_real_id
 * @property string $relation_parent_id
 * @property string $relation_parent_real_id
 * @property string $remote_ip
 * @property string $shipping_method
 * @property string $store_currency_code
 * @property string $store_name
 * @property string $x_forwarded_for
 * @property string $customer_note
 * @property string $created_at
 * @property string $updated_at
 * @property integer $total_item_count
 * @property integer $customer_gender
 * @property string $hidden_tax_amount
 * @property string $base_hidden_tax_amount
 * @property string $shipping_hidden_tax_amount
 * @property string $base_shipping_hidden_tax_amnt
 * @property string $hidden_tax_invoiced
 * @property string $base_hidden_tax_invoiced
 * @property string $hidden_tax_refunded
 * @property string $base_hidden_tax_refunded
 * @property string $shipping_incl_tax
 * @property string $base_shipping_incl_tax
 * @property string $coupon_rule_name
 * @property integer $paypal_ipn_customer_notified
 * @property integer $gift_message_id
 * @property string $base_customer_balance_amount
 * @property string $customer_balance_amount
 * @property string $base_customer_balance_invoiced
 * @property string $customer_balance_invoiced
 * @property string $base_customer_balance_refunded
 * @property string $customer_balance_refunded
 * @property string $bs_customer_bal_total_refunded
 * @property string $customer_bal_total_refunded
 * @property string $gift_cards
 * @property string $base_gift_cards_amount
 * @property string $gift_cards_amount
 * @property string $base_gift_cards_invoiced
 * @property string $gift_cards_invoiced
 * @property string $base_gift_cards_refunded
 * @property string $gift_cards_refunded
 * @property integer $gw_id
 * @property integer $gw_allow_gift_receipt
 * @property integer $gw_add_card
 * @property string $gw_base_price
 * @property string $gw_price
 * @property string $gw_items_base_price
 * @property string $gw_items_price
 * @property string $gw_card_base_price
 * @property string $gw_card_price
 * @property string $gw_base_tax_amount
 * @property string $gw_tax_amount
 * @property string $gw_items_base_tax_amount
 * @property string $gw_items_tax_amount
 * @property string $gw_card_base_tax_amount
 * @property string $gw_card_tax_amount
 * @property string $gw_base_price_invoiced
 * @property string $gw_price_invoiced
 * @property string $gw_items_base_price_invoiced
 * @property string $gw_items_price_invoiced
 * @property string $gw_card_base_price_invoiced
 * @property string $gw_card_price_invoiced
 * @property string $gw_base_tax_amount_invoiced
 * @property string $gw_tax_amount_invoiced
 * @property string $gw_items_base_tax_invoiced
 * @property string $gw_items_tax_invoiced
 * @property string $gw_card_base_tax_invoiced
 * @property string $gw_card_tax_invoiced
 * @property string $gw_base_price_refunded
 * @property string $gw_price_refunded
 * @property string $gw_items_base_price_refunded
 * @property string $gw_items_price_refunded
 * @property string $gw_card_base_price_refunded
 * @property string $gw_card_price_refunded
 * @property string $gw_base_tax_amount_refunded
 * @property string $gw_tax_amount_refunded
 * @property string $gw_items_base_tax_refunded
 * @property string $gw_items_tax_refunded
 * @property string $gw_card_base_tax_refunded
 * @property string $gw_card_tax_refunded
 * @property integer $reward_points_balance
 * @property string $base_reward_currency_amount
 * @property string $reward_currency_amount
 * @property string $base_rwrd_crrncy_amt_invoiced
 * @property string $rwrd_currency_amount_invoiced
 * @property string $base_rwrd_crrncy_amnt_refnded
 * @property string $rwrd_crrncy_amnt_refunded
 * @property integer $reward_points_balance_refund
 * @property integer $reward_points_balance_refunded
 * @property integer $reward_salesrule_points
 * @property string $onestepcheckout_order_comment
 * @property string $onestepcheckout_giftwrap_amount
 */
class Mage1SalesOrder extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_flat_order}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
