<?php

/**
 * This is the model class for table "salesrule_product_attribute".
 *
 * The followings are the available columns in table 'salesrule_product_attribute':
 * @property string $rule_id
 * @property integer $website_id
 * @property integer $customer_group_id
 * @property integer $attribute_id
 */
class Mage1SalesruleProductAttribute extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{salesrule_product_attribute}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesruleProductAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
