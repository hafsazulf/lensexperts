<?php

/**
 * This is the model class for table "sales_order_tax_item".
 *
 * The followings are the available columns in table 'sales_order_tax_item':
 * @property string $tax_item_id
 * @property string $tax_id
 * @property string $item_id
 * @property string $tax_percent
 */
class Mage1SalesOrderTaxItem extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_order_tax_item}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1SalesOrderTaxItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
