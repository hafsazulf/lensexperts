<?php

/**
 * This is the model class for table "review_store".
 *
 * The followings are the available columns in table 'review_store':
 * @property string $review_id
 * @property integer $store_id
 */
class Mage1ReviewStore extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{review_store}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1ReviewStore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
