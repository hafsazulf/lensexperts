<?php

/**
 * This is the model class for table "catalog_category_product".
 *
 * The followings are the available columns in table 'catalog_category_product':
 * @property string $category_id
 * @property string $product_id
 * @property integer $position
 */
class Mage1CatalogCategoryProduct extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_category_product}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogCategoryProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
