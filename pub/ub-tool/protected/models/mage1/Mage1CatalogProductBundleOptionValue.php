<?php

/**
 * This is the model class for table "catalog_product_bundle_option_value".
 *
 * The followings are the available columns in table 'catalog_product_bundle_option_value':
 * @property string $value_id
 * @property string $option_id
 * @property integer $store_id
 * @property string $title
 */
class Mage1CatalogProductBundleOptionValue extends Mage1ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_product_bundle_option_value}}';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mage1CatalogProductBundleOptionValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
